FROM nginx:latest

LABEL maintainer  Vhico Putra Pratama <vhicoputra.vp@gmail.com>

COPY ./landing-page-master/ /usr/share/nginx/html/